<?php
/**
 * 404 Page
 */
get_header(); ?>

<img src="/wp-content/uploads/2020/01/404-Banner-Turkey-04.jpg" alt="404 Image" class="banner-404">

<section class="block">
    <div class="container not-found-wrap">
        <div class="txt-center">
            <h1 class="block-title">404</h1>
            <h2 class="block-title">OH CRUMBS...</h2>
            <p>We are sorry, but this page doesn't exist. <br>
            Please check entered address or go to our homepage to find infinite possibilities.</p>
            <div class="btn-wrap">
                <a class="btn btn-outline-red" href="<?= site_url('/');?>">GO HOME</a>
            </div>
        </div>

    </div>
</section>

<?php get_footer();


