<?php 

// vars
$countryCode = get_field('Country_Code');
$image1 = get_field('Digital_Features_and_Benefits_1');
$image2 = get_field('Digital_Features_and_Benefits_2');
$image3 = get_field('Digital_Features_and_Benefits_3');
$description = get_field('RomanceDescription');
$descriptionLocal = get_field('RomanceDescriptionLocal');
$ingredients = get_field('IngredientListingLocal');
$sku = get_field('ProductCode');
$handlingLocal = get_field('HandlingInstructionsLocal');

// extra field not shown in turkey
$handling = get_field('HandlingInstructions');
$handlingArr = explode(";", $handling);

$servingSize = get_field('ServingSize');
$servingSizeUOM = get_field('ServingSizeUOM');
$shelfLifefromMfg = get_field('ShelfLifefromMfg');
$storage = get_field('Storage');
$shelfLifeRefrigeratedPrepared = get_field('ShelfLifeRefrigeratedPrepared');
$shelfLifeUOM = get_field('ShelfLifeUOM');
$storageLocal = get_field('StorageLocal');

$allergens = get_field('allergens');
$allergensArr = json_decode($allergens, true);
$allergensContainsArray = array();
$allergensMayContainArray = array();

// extra field in India?
$allergensFinalArray = array();


$packagings = get_field('packagings');
$packagingsArr = json_decode($packagings, true);
$claims = get_field('claims');
$claimsArr = json_decode($claims, true);
$categories = get_the_terms( $post->ID , 'product_categories' );
$tags = get_the_terms( $post->ID, 'post_tag' ); 
$parent = get_term_parents_list( $categories[0]->term_id, 'product_categories', array('link' => false) );
$parentName = explode("/", $parent);
$parentSlug = explode("/", $parent);
$productId = $post->ID;

foreach ($packagingsArr as $key => $value ) { 
    $PalletBlockNumberofcases = $value["PalletBlockNumberofcases"];
    $PalletHeightNumberofcases = $value["PalletHeightNumberofcases"];
    $UnitsPerCase = $value["UnitsPerCase"];
    $NetWeightAmountMetric = $value["NetWeightAmountMetric"];
    $GrossWeightMetricUOM = $value["GrossWeightMetricUOM"];
}

foreach ($claimsArr as $key => $value ) { 
    $ClaimCode = $value["ClaimCode"];
}

foreach ( $allergensArr as $key => $value ) { 
    if ($value["AllergenLevelofContainmentCodeFromRecipe"] === "10") {
        array_push($allergensContainsArray, $value["AllergenDescription"]);
    } else if ($value["AllergenLevelofContainmentCodeFromRecipe"] === "20") {
        array_push($allergensMayContainArray, $value["AllergenDescription"]);
    } else {
        // do nothing
    }


}
// ------------------------------------------ BEGINNING OF PAGE ------------------------------------------------

get_header(); 


?>

<div class="product-breadcrumbs" id="crumb">
    <div class="container">
        <?php if ($countryCode === "IN") { ?>
            <a href="<?php echo get_home_url(); ?>/our-products">Products</a>
        <?php

        } else if ($countryCode === "TR") { ?>
            <a href="<?php echo get_home_url(); ?>/urunlerimiz">ÜRÜNLERİMİZ</a>
        <?php

        } else if ($countryCode === "PE") { ?>
            <a href="<?php echo get_home_url(); ?>/our-products">Products</a>
        <?php

        } else if ($countryCode === "JA") { ?>
            <a href="<?php echo get_home_url(); ?>/our-products">Products</a>
        <?php
        }

        else { ?>
            <a href="<?php echo get_home_url(); ?>/our-products">Products</a>
        <?php } 

      
        if($parentName !== null) { ?>
           / 
            
            <a href="<?php echo get_home_url() . "/product_categories/" . $categories[0]->slug ?>"><?php echo $parentName[0] ?></a>
            <?php
        } 
        ?>
        
    </div>
</div>



<section class="single-product-section">
<div class="container single-product">
        <div class="single-product-grid ie-row">
            <div class="single-product-left col-ie-md-6">
                <div class="content-container row product-image"> 
                        <div id="slider-wrapper">
                        <div id="image-slider">
                            <ul>
                                <li class="active-img">
                                    <img src="<?php echo $image1; ?>" alt="" />
                                </li>
                                <li>
                                    <img src="<?php echo $image2; ?>" alt="" />
                                </li>
                                <li>
                                    <img src="<?php echo $image3; ?>" alt="" />
                                </li>                                           
                            </ul>
                        </div>
                        <div id="thumbnail">
                            <ul>
                                <li class="active col-ie-xs-4"><img src="<?php echo $image1; ?>" alt="" /></li>
                                <li class="col-ie-xs-4"><img src="<?php echo $image2; ?>" alt="" /></li>
                                <li class="col-ie-xs-4"><img src="<?php echo $image3; ?>" alt="" /></li>                
                            </ul>
                        </div>
                        </div>

                </div>
            </div>
            <div class="single-product-right col-ie-md-6">
                <div class="content-container row">
                    <h2 class="product-title"><?php echo get_the_title($post_object->ID); ?></h2>
                    <p class="product-description"><?php echo $description;  ?> </p>
                </div>
                <div class="section-separator"></div>
                <div class="content-container row product-details">
                    <div class="left-col">
                        <?php 
                        if ($UnitsPerCase !== NULL) { ?>
                            <div>
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/includes/Icons/pack-size.svg" alt='placeholder' />
                                <p>
                                    <?php if ($countryCode === "IN") { ?>
                                        Units per case
                                    <?php

                                    } else if ($countryCode === "TR") { ?>
                                       Koli İçeriği
                                    <?php

                                    } else if ($countryCode === "PE") { ?>
                                       Pack Size 
                                    <?php

                                    } else if ($countryCode === "JA") { ?>
                                        Pack Size
                                    <?php
                                    }
                                    else { ?>
                                        Pack Size
                                    <?php } ?>

                                    <span><?php echo $UnitsPerCase; ?></span></p>
                            </div> <?php 
                        }
                        if ($PalletBlockNumberofcases !== NULL  || $PalletHeightNumberofcases !== NULL) { ?>
                            <div>
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/includes/Icons/pallet-config.svg" alt='placeholder' />
                                <p>
                                    <?php if ($countryCode === "IN") { ?>
                                        Pallet Config
                                    <?php

                                    } else if ($countryCode === "TR") { ?>
                                       Palet Detay
                                    <?php

                                    } else if ($countryCode === "PE") { ?>
                                       Pallet Config 
                                    <?php

                                    } else if ($countryCode === "JA") { ?>
                                        Pallet Config 
                                    <?php
                                    }
                                    else { ?>
                                        Pallet Config 
                                    <?php } ?>

                                    <span><?php echo $PalletBlockNumberofcases . " x " . $PalletHeightNumberofcases; ?></span></p>

                            </div> <?php 
                        } if ($NetWeightAmountMetric !== NULL  || $GrossWeightMetricUOM !== NULL) { ?>
                            <div>
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/includes/Icons/pallet-config.svg" alt='placeholder' />
                                    <p> 
                                    <?php if ($countryCode === "IN") { ?>
                                        Case Weight
                                    <?php

                                    } else if ($countryCode === "TR") { ?>
                                       Koli Ağırlığı
                                    <?php

                                    } else if ($countryCode === "PE") { ?>
                                       Weight
                                    <?php

                                    } else if ($countryCode === "JA") { ?>
                                        Weight
                                    <?php
                                    }
                                    else { ?>
                                        Weight
                                    <?php } ?>
                                    <span><?php echo $NetWeightAmountMetric . " " . $GrossWeightMetricUOM; ?></span></p>

                            </div> <?php 
                        } ?>
                    </div>
                    <div class="right-col">
                    <?php                     
                        if ($shelfLifeRefrigeratedPrepared != "") { ?>
                            <div>
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/includes/Icons/refrigeration-shelf-life.svg" alt='placeholder' />
                                    <p>
                                    <?php if ($countryCode === "IN") { ?>
                                        Refrigerated Shelf Life
                                    <?php

                                    } else if ($countryCode === "TR") { ?>
                                        Raf Ömrü (buzdolabında)
                                    <?php

                                    } else if ($countryCode === "PE") { ?>
                                        Refrigerated Shelf Life
                                    <?php

                                    } else if ($countryCode === "JA") { ?>
                                        Refrigerated Shelf Life
                                    <?php
                                    }
                                    else { ?>
                                        Refrigerated Shelf Life
                                    <?php } ?>


                                    <?php if ($countryCode === "IN") { ?>
                                        
                                        <span><?php echo $shelfLifeRefrigeratedPrepared; 
                                        if($shelfLifeUOM < 2 ) {
                                            ?>  day<?php
                                            } else {
                                                ?>  days<?php
                                            }?>
                                        </span>

                                    <?php

                                    } else if ($countryCode === "TR") { ?>

                                        <span><?php echo $shelfLifeRefrigeratedPrepared; 
                                        if($shelfLifeUOM < 2 ) {
                                            ?>  gün<?php
                                            } else {
                                                ?>  gün<?php
                                            }?>
                                        </span>

                                    <?php

                                    } else if ($countryCode === "PE") { ?>
                                       
                                        <span><?php echo $shelfLifeRefrigeratedPrepared; 
                                        if($shelfLifeUOM < 2 ) {
                                            ?>  day<?php
                                            } else {
                                                ?>  days<?php
                                            }?>
                                        </span>

                                    <?php

                                    } else if ($countryCode === "JA") { ?>
                                        
                                        <span><?php echo $shelfLifeRefrigeratedPrepared; 
                                        if($shelfLifeUOM < 2 ) {
                                            ?>  day<?php
                                            } else {
                                                ?>  days<?php
                                            }?>
                                        </span>

                                    <?php
                                    }
                                    else { ?>
                                        
                                        <span><?php echo $shelfLifeRefrigeratedPrepared; 
                                        if($shelfLifeUOM < 2 ) {
                                            ?>  day<?php
                                            } else {
                                                ?>  days<?php
                                            }?>
                                        </span>

                                    <?php } ?>

                                    <!--  <span> <?php echo $shelfLifeRefrigeratedPrepared . " " . $shelfLifeUOM; ?></span> -->
                                 </p>

                            </div><?php 
                        } 
                        if ($shelfLifefromMfg !== NULL  || $shelfLifeUOM !== NULL) { ?>
                            <div>
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/includes/Icons/frozen-shelf-life.svg" alt='placeholder' />
                                    <p>
                                    <?php if ($countryCode === "IN") { ?>
                                        Frozen Shelf Life
                                    <?php

                                    } else if ($countryCode === "TR") { ?>
                                        Raf Ömrü (donuk)
                                    <?php

                                    } else if ($countryCode === "PE") { ?>
                                        Frozen Shelf Life
                                    <?php

                                    } else if ($countryCode === "JA") { ?>
                                        Frozen Shelf Life
                                    <?php
                                    }
            
                                    else { ?>
                                        Frozen Shelf Life
                                    <?php } ?>


                                    <!-- Field name -->
                                    <?php if ($countryCode === "IN") { ?>
                                        
                                        <span>
                                          <?php echo $shelfLifefromMfg; 
                                        if($shelfLifeUOM === "MON" ) {
                                            ?> months<?php
                                            
                                            } else {
                                                ?> days<?php
                                            }?>
                                        </span>

                                       <!-- <span><?php echo $shelfLifefromMfg . " " . $shelfLifeUOM; ?></span>  -->
                                       

                                    <?php

                                    } else if ($countryCode === "TR") { ?>
                                        
                                        <span><?php echo $shelfLifefromMfg; 
                                        if($shelfLifefromMfg < 2 ) {
                                            ?>  gün<?php
                                            

                                            } else {
                                                ?>  gün<?php
                                            }?>
                                        </span>

                                    <?php

                                    } else if ($countryCode === "PE") { ?>
                                        
                                        <span><?php echo $shelfLifefromMfg; 
                                        if($shelfLifefromMfg < 2 ) {
                                            ?>  day<?php
                                            

                                            } else {
                                                ?>  days<?php
                                            }?>
                                        </span>

                                    <?php

                                    } else if ($countryCode === "JA") { ?>
                                        
                                        <span><?php echo $shelfLifefromMfg; 
                                        if($shelfLifefromMfg < 2 ) {
                                            ?>  day<?php
                                            

                                            } else {
                                                ?>  days<?php
                                            }?>
                                        </span>

                                    <?php
                                    }
                                    else { ?>

                                        <span><?php echo $shelfLifefromMfg; 
                                        if($shelfLifefromMfg < 2 ) {
                                            ?>  day<?php
                                            

                                            } else {
                                                ?>  days<?php
                                            }?>
                                        </span>
                                        
                                    <?php } ?>


                                     <!-- <span><?php echo $shelfLifefromMfg . " " . $shelfLifeUOM; ?></span>  -->
                                </p>
                            </div><?php 
                        } ?>
                    </div>
                </div>
                <div class="section-separator"></div>
                <div class="content-container row">
                    <p>SKU Code: <?php echo $sku; ?></p>
                    <?php 
                        if ($categories) { ?>
                            <p>Categories: <?php foreach ( $categories as $item ) {  ?>
                                <?php echo $item->name . " ";  ?> <?php
                            } ?></p>
                             <?php 
                        }
                        ?>
                    <?php 
                        if ($tags) { ?>
                            <p>Tags: <?php foreach ( $tags as $item ) {  ?>
                                <?php echo $item->name . " ";  ?> <?php
                            } ?></p>  <?php 
                        }
                        ?>
                </div>
                <?php 
                    if ($ingredients !== "" ) { ?>
                        <div class="section-separator"></div>
                        <div class="content-container row">


                            <?php if ($countryCode === "IN") { ?>
                                <h2><?php _e( 'INGREDIENTS' ); ?></h2> 
                            <?php

                            } else if ($countryCode === "TR") { ?>
                                <h2><?php _e( 'İÇİNDEKİLER' ); ?></h2>
                            <?php

                            } else if ($countryCode === "PE") { ?>
                                <h2><?php _e( 'INGREDIENTS PERU' ); ?></h2>
                            <?php

                            } else if ($countryCode === "JA") { ?>
                                <h2><?php _e( 'INGREDIENTS JAPAN' ); ?></h2>
                            <?php
                            }

                            else { ?>
                                <h2><?php _e( 'INGREDIENTS' ); ?></h2>
                            <?php } ?>                        

                            <p class="product-ingredients"><?php var_dump($ingredients) ;echo $ingredients; ?></p>
                        </div> <?php 
                    }?>
            </div>
        </div>
        <div class="single-product-tabs">
            <div id="tabs">
                <ul> <?php
                    if ($handling !== "" ) { ?>
                        <li><a href="#tabs-1">
                            <?php if ($countryCode === "IN") { ?>
                                <?php _e( 'TIPS & HANDLING' ); ?>
                            <?php

                            } else if ($countryCode === "TR") { ?>
                                <?php _e( 'KULLANIM TALİMATLARI' ); ?>
                            <?php

                            } else if ($countryCode === "PE") { ?>
                                <?php _e( 'TIPS & HANDLING PERU' ); ?>
                            <?php

                            } else if ($countryCode === "JA") { ?>
                                <?php _e( 'TIPS & HANDLING JAPAN' ); ?>
                            <?php
                            }
    
                            else { ?>
                                <?php _e( 'TIPS & HANDLING' );?>
                            <?php } ?>
                        </a>
                        </li>  <?php 
                    }

                    if (!empty($allergensContainsArray) || !empty($allergensMayContainArray)) { ?>
                         <li>
                            <a href="#tabs-2">
                                <?php if ($countryCode === "IN") { ?>
                                    <?php _e( 'ALLERGENS' ); ?>
                                    <?php

                                    } else if ($countryCode === "TR") { ?>
                                        <?php _e( 'ALERJENLER' ); ?>
                                    <?php

                                    } else if ($countryCode === "PE") { ?>
                                        <?php _e( 'ALLERGENS PERU' ); ?>
                                    <?php

                                    } else if ($countryCode === "JA") { ?>
                                        <?php _e( 'ALLERGENS JAPAN' ); ?>
                                    <?php
                                    }
            
                                    else { ?>
                                        <?php _e( 'ALLERGENS' ); ?>
                                <?php } ?>
                            </a>
                            </li> <?php 
                    }?>

                             <li>
                            <a href="#tabs-3">
                                <?php if ($countryCode === "IN") { ?>
                                       <?php _e( 'RECIPES' ); ?>
                                    <?php

                                    } else if ($countryCode === "TR") { ?>
                                         <?php _e( 'RECIPES' ); ?>
                                    <?php

                                    } else if ($countryCode === "PE") { ?>
                                         <?php _e( 'RECIPES' ); ?>
                                    <?php

                                    } else if ($countryCode === "JA") { ?>
                                         <?php _e( 'RECIPES' ); ?>
                                    <?php
                                    }
            
                                    else { ?>
                                        <?php _e( 'RECIPES' ); ?>
                                <?php } ?>
                            </a>
                            </li>            
                </ul> <?php 
                if ($handling !== "" ) { ?>
                    <div id="tabs-1">
                        <ul> <?php 
                           foreach ( $handlingArr as $item ) {  ?>
                                <li>- <?php echo $item; ?></li> <?php
                            } ?>
                        </ul>
                     </div>  <?php 
                }


                if (!empty($allergensContainsArray) || !empty($allergensMayContainArray)) { ?>

                <div id="tabs-2">
                    <div> <?php
                             
                                
                            
                            if ($allergensContainsArray) { ?>
                                <p>
                                <?php if ($countryCode === "IN") { ?>
                                    Contains: 
                                <?php

                                } else if ($countryCode === "TR") { ?>
                                    Contains:
                                  
                                <?php

                                } else if ($countryCode === "PE") { ?>
                                    Contains: 
                                <?php

                                } else if ($countryCode === "JA") { ?>
                                    Contains: 
                                <?php
                                }

                                else { ?>
                                    Contains: 
                                <?php }
                                
                                ?>

                                <?php  echo htmlentities(implode(", ", $allergensContainsArray)); ?> </p> <?php


                            } else if ($allergensMayContainArray) { ?>

                                <p>
                                <?php if ($countryCode === "IN") { ?>
                                    May Contain:
                                <?php

                                } else if ($countryCode === "TR") { ?>
                                    
                                <?php

                                } else if ($countryCode === "PE") { ?>
                                    May Contain:
                                <?php

                                } else if ($countryCode === "JA") { ?>
                                    May Contain:
                                <?php
                                }

                                else { ?>
                                    May Contain:
                                <?php } ?>


                                <?php echo htmlentities(implode(", ", $allergensMayContainArray)); ?></p> 
                                <?php

                                } 

                            ?>
                        </div>

                        <!-- For tuekry and Japan -->
                        <!-- <?php if( get_field('allergens_bm') ): ?>
                            <div>
                                <span><?php the_field('allergens_bm'); ?></span>
                            </div>
                        <?php endif; ?> -->


                    </div> <?php
                }
                ?>
                <div id="tabs-3">
                    <div class="product-list">
                    <?php
                       
                    global $post;
                    $args = array( 'numberposts' => -1, 'post_type'=> 'recipes');
                    $posts = get_posts($args);
                    $thereAreRecipes = false;
                  
                    
                    foreach( $posts as $post ) : setup_postdata($post); 
                        // var_dump($post);
                        $postId = $post->post_ID;
                      
                        $featured_img_url = get_the_post_thumbnail_url($postId,'full');
                        $postTitle = $post->post_title;
                        $postName = $post->post_name;
                        $postidchantier = $post->ID;
                        $relatedProducts = get_field('related_products', $postidchantier);
                     
                        if ($relatedProducts) { ?>
                            <!-- <div class="product-list">  --><?php
                            foreach ($relatedProducts as $relatedProduct) {
                                foreach ($relatedProduct as $product) {  
                                    if ($product->ID == $productId) {
                                        $thereAreRecipes = true;
                                        ?>


                                        <div class="product-list-item">

                                            <?php if ( is_user_logged_in() ) { ?>
                                            <a href="<?php echo get_home_url() . "/recipe/" . $postName; ?>">
                                                <div class="image-container">
                                                    <img src="<?php echo $featured_img_url ?>" alt="product" />
                                                </div>
                                                <h3><?php echo $postTitle; ?></h3>
                                            </a>
                                            <?php } else { ?>
                                            <a  href="<?php echo get_home_url() . "/user/"; ?>">
                                                <div class="image-container">
                                                    <img class="category-lock category-product-lock" src="<?php echo get_stylesheet_directory_uri(); ?>/recipe-includes/images/lock-alt-solid.svg" alt='placeholder' />
                                                    <img src="<?php echo $featured_img_url ?>" alt="product" />
                                                </div>
                                                <h3><?php echo $postTitle; ?></h3>
                                            </a>
                                            <?php } ?>

                                        </div>
                                        
                                        <?php
                                    } 
                                }
                            } ?>
                            <!-- </div> -->
                        <?php } 
                endforeach; 
                    ?>
                    </div>
                </div>
 
            </div>
        </div>
        <div class="related-products container">

            <?php if ($countryCode === "IN") { ?>
               <h2>RELATED PRODUCTS</h2>
            <?php

            } else if ($countryCode === "TR") { ?>
                <h2>RELATED PRODUCTS</h2>
            <?php

            } else if ($countryCode === "PE") { ?>
                <h2>RELATED PRODUCTS</h2>
            <?php

            } else if ($countryCode === "JA") { ?>
                <h2>RELATED PRODUCTS</h2>
            <?php
            }

            else { ?>
                <h2>RELATED PRODUCTS</h2>
           <?php } ?>

            
            <div class="product-list">
            <?php 
             $categoryArgs = array(
                'post_type'   => 'product',
                'post_status' => 'publish',
                'posts_per_page' => 3,
                'post__not_in' => array( $post->ID ),
                'tax_query'   => array(
                    array(
                        'taxonomy' => 'product_categories',
                        'field'    => 'slug',
                        'terms'    => $categories[0]->slug
                    )
                )
            );
            $categoryProducts = new WP_Query( $categoryArgs );
            while( $categoryProducts->have_posts() ) :
                $categoryProducts->the_post();
                ?>
                <div class="product-list-item">
                    <a href="<?php echo get_the_permalink(); ?>">
                    <div class="image-container">
                    <?php if (has_post_thumbnail( $post->ID ) ) {
                        the_post_thumbnail(); 
                    } else { ?>
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/includes/placeholder.jpg" alt='placeholder' />
                        <?php
                    } ?>
                    </div>
                        <h3 class="product-title"><?php echo get_the_title(); ?></h3>
                        <?php $member_tags = get_the_terms( get_the_ID(), 'post_tag' ); 
                        foreach ( $member_tags as $tag) { 
                            if ($tag->name == "New") {?>
                                <div class="product-tag">
                                    <span><?php echo $tag->name; ?></span>
                                </div>
                                <?php
                            }
                        }
                        ?>     
                    </a>
                </div> 
            <?php
            endwhile;
            ?>
            </div> 
        </div>
</section>
<?php


get_footer();
