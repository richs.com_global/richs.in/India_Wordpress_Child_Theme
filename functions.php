<?php
define('CHILD_THEME_URL', get_stylesheet_directory_uri());
define('CHILD_THEME_DIR', get_stylesheet_directory());

/**
 * Child Theme constants
 * You can change below constants
 */

// white label

define('WHITE_LABEL', false);

/**
 * Enqueue Styles
 */



function mfnch_enqueue_styles()
{
	// enqueue the parent stylesheet
	// however we do not need this if it is empty
	// wp_enqueue_style('parent-style', get_template_directory_uri() .'/style.css');

	// enqueue the parent RTL stylesheet

	if (is_rtl()) {
		wp_enqueue_style('mfn-rtl', get_template_directory_uri() . '/rtl.css');
	}

	// enqueue the child stylesheet

	wp_dequeue_style('style');
	wp_enqueue_style('style', get_stylesheet_directory_uri() .'/style.css');
}
add_action('wp_enqueue_scripts', 'mfnch_enqueue_styles', 101);


//////////////////////////////////////////////////////////////////////////////////////////////////
/// CUSTOMISE LOGIN PAGE
//////////////////////////////////////////////////////////////////////////////////////////////////


function tbf_login_logo() { ?>
    <style type="text/css">
        body.login {
            /*background:#fff;*/
            background-image: url("https://www.richtk.benchmarkdev.co.za/wp-content/uploads/2020/02/Join-Our-Family-Banner2-scaled.jpg");
            background-position: center; /* Center the image */
            background-repeat: no-repeat; /* Do not repeat the image */
            background-size: cover; /* Resize the background image to cover the entire container */
        }
        .login #login_error, .login .message {
            background:#fff !important;
            border-top:1px solid rgba(255,255,255,0.3);
            border-right:1px solid rgba(255,255,255,0.3);
            border-bottom:1px solid rgba(255,255,255,0.3);
            border-left:4px solid #D31145 !important;
            border-radius:5px;
            color: #080c19;
        }
        .login h1 {
            margin:0 0 10px 0;
            padding-left: 20px;
        }
        @media only screen and (max-width: 767px) {
            #login {
            padding: 100px 0 0;
            width: 320px;
            padding: 8% 0% 0 !important;
            margin: auto;
            }
        }
        @media only screen and (min-width: 768px) {
            #login {
                padding: 100px 0 0;
                width: 320px;
                padding: 8% 18% 0 !important;
                margin: auto;
                float: right;
            }
        }
        #login h1 a {
            width:100%;
            height:73px !important;
            background-size: 100%;
            background-image: url("https://www.richtk.benchmarkdev.co.za/wp-content/uploads/2020/01/Richs-logo.svg");
        }
        #nav {
            text-align:center;
            color: #D31145 !important;
            width: 100%;
        }
        #wp-submit {
            display: block;
            width: 100%;
            padding: 10px;
            height: 50px;
        }
        #nav a,
        #backtoblog a {
            color: #D31145 !important;
            text-decoration: none;
            width: 100% !important;
        }
        .login form {
            background:#fff !important;
            border-radius:5px 5px 0px 0px;
            width:100%;
            box-shadow: none !important;
            border: 0px !important;
            padding: 26px 24px 20px !important;
/*          border: 1px solid #ccd0d4;
            box-shadow: 0 1px 3px rgba(0,0,0,.04);*/
        }
        .g-recaptcha {
            margin-left: auto !important;
            margin-right: auto !important;
            display: block !important;
            width: 100% !important;
        }
        .wp-core-ui .button-primary, .submit input#wp-submit:disabled {
            background: red !important; 
            background:#D31145 !important;
            border-color: #D31145 #D31145 #D31145 !important;
            box-shadow: none !important;
            text-transform:uppercase !important;
            text-shadow: none !important;
            color: #fff !important;
        }
        .login a.privacy-policy-link {
                display: none;
            }
        #backtoblog {
            text-align: center;
            width: 100% !important;
        }
        .apsl-login-new-text {
            font-size: 16px;
            font-weight: 300;
            line-height: 48px;
            width: 100%;
            overflow: hidden;
        }
        .apsl-login-new-text:before,
        .apsl-login-new-text:after {
            content: '';
            display: inline-block;
            vertical-align: middle;
            width: calc(50% - 15px) !important;
            height: 3px;
            border-top: 1px solid #ddd;
        }
        .social-networks {
            text-align: center;
        }
        .forgetmenot {
            margin: 20px 0 !important;
            box-sizing: border-box;
            display: block;
            text-align: center !important;
            width: 100%;
        }
        .login #backtoblog {
            background-color: #fff;
        }
        #backtoblog {
            margin: 0px !important;
            border-radius: 0px 0px 5px 5px;
            padding: 10px 24px 40px 24px !important;
        }
        .login #nav {
            background: #fff;
            margin: 0px !important;
        }
    </style>
<?php }

add_action( 'login_enqueue_scripts', 'tbf_login_logo' );

// Change url of logo on wordpress login page
function my_login_logo_url() {
    return get_bloginfo( 'url' );
}
add_filter( 'login_headerurl', 'my_login_logo_url' );

// Change title on wordpress login page
function my_login_logo_url_title() {
    return "RICH's&reg; AFRICA";
}
add_filter( 'login_headertitle', 'my_login_logo_url_title' );


//////////////////////////////////////////////////////////////////////////////////////////////////
// [login_form]
//////////////////////////////////////////////////////////////////////////////////////////////////
add_shortcode('login_form', 'bb_sc_login_form');

// The callback function that will replace [login_form]
function bb_sc_login_form($atts, $content) {

    $url = filter_input(INPUT_SERVER, 'REQUEST_URI');
    $redirect_to = strstr($url, 'https://', false);

    $args = array(
        'echo'           => true,
        'remember'       => true,
        'redirect'       => $redirect_to,
        'form_id'        => 'loginform',
        'id_username'    => 'user_login',
        'id_password'    => 'user_pass',
        'id_remember'    => 'rememberme',
        'id_submit'      => 'submit',
        'label_username' => __( 'Email Address' ),
        'label_password' => __( 'Password' ),
        'label_remember' => __( 'Remember Me' ),
        'label_log_in'   => __( 'Log In' ),
        'value_username' => '',
        'value_remember' => false
    );

    echo '<div class="login-wrapper">';
    echo '<div class="login-form page-restrict-output">';
    if (!is_user_logged_in()) {
        echo '<h2 class="custom-h2-m rich-red uppercase">Member Login</h2>';
        echo  wp_login_form($args);
        echo '<span class="apsl-login-new-text">OR</span>';
        echo do_shortcode('[apsl-login-lite]');
        echo '<p><a href="' . home_url('/register/') . '">Register</a> | <a href="' . home_url('/wp-login.php?action=lostpassword') . '">Lost your password?</a></p>';
    } else {
        global $current_user;
        echo  '<p class="msg-success">Thanks '.$current_user->user_firstname.', you are logged in.</p>';
        echo  '<span class="bb-button block"><a href="' . home_url('/my-account/') . '">Go to your account</a></span>';
    }
    echo  '</div><!--login-form-->';
    echo  '</div><!--login-wrapper-->';

}

//// CHANGE LOGIN URL
//add_filter( 'login_url', 'bb_login_page', 10, 3 );
//function bb_login_page( $login_url, $redirect, $force_reauth ) {
//    return home_url( '/login/' );
//}

//////////////////////////////////////////////////////////////////////////////////////////////////
// [Include Styles & Scripts for Products]
//////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Proper way to enqueue scripts and styles
 */
function wpdocs_theme_name_scripts() {
    wp_enqueue_script ( 'jquery' );
    wp_enqueue_style( 'product_styles', get_stylesheet_directory_uri() . '/includes/product_styles.css' );
    wp_enqueue_script( 'custom-script', get_stylesheet_directory_uri() . '/includes/slider.js', array('jquery'));
    wp_enqueue_style( 'slick_styles', get_stylesheet_directory_uri() . '/includes/slick/slick.css' );
    wp_enqueue_style( 'slick_theme_styles', get_stylesheet_directory_uri() . '/includes/slick/slick-theme.css' );
    wp_enqueue_script( 'slick-script', get_stylesheet_directory_uri() . '/includes/slick/slick.js', array( 'jquery' ) );

}
add_action( 'wp_enqueue_scripts', 'wpdocs_theme_name_scripts' );


//////////////////////////////////////////////////////////////////////////////////////////////////
// [Hide admin bar for all users except admin]
//////////////////////////////////////////////////////////////////////////////////////////////////
/**
*/

add_action('after_setup_theme', 'remove_admin_bar');
 
function remove_admin_bar() {
if (!current_user_can('administrator') && !is_admin()) {
  show_admin_bar(false);
    }
}

/**
 * WordPress function for redirecting users on login based on user role
 */
function my_login_redirect( $url, $request, $user ){
    if( $user && is_object( $user ) && is_a( $user, 'WP_User' ) ) {
        if( $user->has_cap( 'administrator' ) ) {
            $url = home_url('');
        } else {
            $url = home_url('');
        }
    }
    return $url;
}

add_filter('login_redirect', 'my_login_redirect', 10, 3 );


// Disable the toolbar for authors only
// add_filter( 'show_admin_bar', function( $show ) {
//     if ( current_user_can( 'customer' ) ) {
//         return false;
//     }
//     return $show;
// } );

// add_filter(‘show_admin_bar’, ‘__return_false’);


// Block Access to /wp-admin for non admins.



// add_action( 'init', 'blockusers_init' ); 
// function blockusers_init() { if ( is_admin() && ! current_user_can( 'administrator' ) && ! ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) { wp_redirect( home_url() ); exit; } } 



//////////////////////////////////////////////////////////////////////////////////////////////////
// EXCERPT LENGTH BLOG POST
//////////////////////////////////////////////////////////////////////////////////////////////////
/**
*/

function custom_excerpt_length( $length ) {
        return 19;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 997 );




//////////////////////////////////////////////////////////////////////////////////////////////////
// [BIGBrave Functions]
//////////////////////////////////////////////////////////////////////////////////////////////////

function add_theme_scripts() {
    wp_enqueue_script("jquery-ui-tabs");
}
add_action( 'wp_enqueue_scripts', 'add_theme_scripts' );



//////////////////////////////////////////////////////////////////////////////////////////////////
// [REGISTER REDIRECT]
//////////////////////////////////////////////////////////////////////////////////////////////////
/* start  */ 
add_filter( 'register_url', 'custom_register_url' );
function custom_register_url( $register_url )
{
    $register_url = get_permalink( $register_page_id = 3297 );
    return $register_url;
}
/* end */











