<?php 

get_header();

$cat_id= get_queried_object()->term_id;
$cat_name= get_queried_object()->name;
$cat_slug= get_queried_object()->slug;
$cat_description= get_queried_object()->description;;
$categories = get_terms( 'recipes_categories', ['child_of' => $cat_id] );
$tags = get_the_terms( $id, 'post_tag');

$parent = get_term_parents_list( $categories[0]->term_id, 'recipes_categories', array('link' => false) );
$parentName = explode("/",$parent);

$args = array(
    'type' => 'recipes',
    'orderby' => 'name',
    'order' => 'ASC'
);
$tags = get_tags($args);

?>

<div class="product-breadcrumbs" id="crumb">
    <div class="container">
        <a href="<?php echo get_home_url(); ?>/our-recipes">Recipes</a>
        <?php if($parentName !== null) {
            ?>
            / 
            <a href="<?php echo get_home_url() . "/recipes_categories/" .  $parentName[0]; ?>"><?php echo $parentName[0] ?></a>
            <?php
        }
        ?>
    </div>
</div>

<div class="container category-template"> <?php

    if (!empty($categories))  { ?>
        <div class="recipe-category-page">
            <div class="top-section">
                <h1><?php echo $cat_name ?></h1>
                <p><?php echo $cat_description ?></p>
            </div>
            <div id="tabs">
                <ul>
                    <li><a href="#all-categories">All</a></li>
                    <?php
                    foreach ( $categories as $category ) {  ?>
                    <li><a href="#<?php echo $category->slug; ?>"><?php echo $category->name; ?> </a></li>
                    <?php
                    } ?>
                </ul>

                <div id="all-categories">

                        <div class="product-list"> <?php
                            $categoryArgs = array(
                                'post_type'   => 'recipes',
                                'post_status' => 'publish',
                                'posts_per_page' => -1,
                                'tax_query'   => array(
                                    array(
                                        'taxonomy' => 'recipes_categories',
                                        'field'    => 'slug',
                                        'terms'    => $cat_slug
                                    )
                                )
                            );
                            $categoryProducts = new WP_Query( $categoryArgs );
                            while( $categoryProducts->have_posts() ) :
                                $categoryProducts->the_post();
                                ?>
                                <div class="product-list-item">
                                    <a href="<?php echo get_the_permalink(); ?>">
                                    <?php if (has_post_thumbnail( $post->ID ) ) {
                                        the_post_thumbnail(); 
                                    } else { ?>
                                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/includes/placeholder.jpg" alt='placeholder' />
                                        <?php
                                    } ?>
                                        <h3><?php echo get_the_title(); ?></h3>
                                        <?php $member_tags = get_the_terms( get_the_ID(), 'post_tag' ); 
                                        foreach ( $member_tags as $tag) { 
                                            if ($tag->name == "New") {?>
                                                <div class="product-tag">
                                                    <span><?php echo $tag->name; ?></span>
                                                </div>
                                                <?php
                                            }
                                        }
                                        ?>     
                                    </a>
                                </div> 
                            <?php
                            endwhile;?>
                        </div>      
                    </div> 

                <?php
                foreach ( $categories as $category ) {  ?>
                    <div id="<?php echo $category->slug; ?>">
                        <div class="product-list"> <?php
                            $categoryArgs = array(
                                'post_type'   => 'recipes',
                                'post_status' => 'publish',
                                'posts_per_page' => -1,
                                'tax_query'   => array(
                                    array(
                                        'taxonomy' => 'recipes_categories',
                                        'field'    => 'slug',
                                        'terms'    => $category->slug
                                    )
                                )
                            );
                            $categoryProducts = new WP_Query( $categoryArgs );
                            while( $categoryProducts->have_posts() ) :
                                $categoryProducts->the_post();
                                ?>
                                <div class="product-list-item">
                                    <a href="<?php echo get_the_permalink(); ?>">
                                    <?php if (has_post_thumbnail( $post->ID ) ) {
                                        the_post_thumbnail(); 
                                    } else { ?>
                                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/includes/placeholder.jpg" alt='placeholder' />
                                        <?php
                                    } ?>
                                        <h3><?php echo get_the_title(); ?></h3>
                                        <?php $member_tags = get_the_terms( get_the_ID(), 'post_tag' ); 
                                        foreach ( $member_tags as $tag) { 
                                            if ($tag->name == "New") {?>
                                                <div class="product-tag">
                                                    <span><?php echo $tag->name; ?></span>
                                                </div>
                                                <?php
                                            }
                                        }
                                        ?>     
                                    </a>
                                </div> 
                            <?php
                            endwhile;?>
                        </div> 
                                
                    </div> 
                <?php
                } ?>
            </div> 
        </div> 
    <?php
    } else { 
        ?>
        <div class="recipe-category-page">
            <div class="top-section">
                <h1><?php echo $cat_name ?></h1>
                <p><?php echo $cat_description ?></p>
            </div>

                <div id="all-categories">

                        <div class="product-list"> <?php
                            $categoryArgs = array(
                                'post_type'   => 'recipes',
                                'post_status' => 'publish',
                                'posts_per_page' => -1,
                                'tax_query'   => array(
                                    array(
                                        'taxonomy' => 'recipes_categories',
                                        'field'    => 'slug',
                                        'terms'    => $cat_slug
                                    )
                                )
                            );
                            $categoryProducts = new WP_Query( $categoryArgs );
                            while( $categoryProducts->have_posts() ) :
                                $categoryProducts->the_post();
                                ?>
                                <div class="product-list-item">
                                    <a href="<?php echo get_the_permalink(); ?>">
                                    <?php if (has_post_thumbnail( $post->ID ) ) {
                                        the_post_thumbnail(); 
                                    } else { ?>
                                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/includes/placeholder.jpg" alt='placeholder' />
                                        <?php
                                    } ?>
                                        <h3><?php echo get_the_title(); ?></h3>
                                        <?php $member_tags = get_the_terms( get_the_ID(), 'post_tag' ); 
                                        foreach ( $member_tags as $tag) { 
                                            if ($tag->name == "New") {?>
                                                <div class="product-tag">
                                                    <span><?php echo $tag->name; ?></span>
                                                </div>
                                                <?php
                                            }
                                        }
                                        ?>     
                                    </a>
                                </div> 
                            <?php
                            endwhile;?>
                        </div>      
                    </div> 
        </div> 
    <?php
    }
    ?>
</div>



<?php 
get_footer();